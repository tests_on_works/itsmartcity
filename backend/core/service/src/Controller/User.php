<?php

namespace App\Controller;

use App\Repository\Core\UsersRepository;
use App\Tools\FakeData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class User extends AbstractController
{
    private Request $request;
    private UsersRepository $usersRepository;

    public function __construct(UsersRepository $usersRepository, RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->usersRepository = $usersRepository;
    }

    #[Route('/users', name: 'users', methods: ['GET'])]
    public function users(): JsonResponse
    {
        $offset = $this->request->query->get('page');
        $users = $this->usersRepository->getUsers($offset);
        return new JsonResponse($users);
    }

    #[Route('/usersByField', name: 'usersByField', methods: ['GET'])]
    public function usersByField(): JsonResponse
    {
        $users = $this->usersRepository->getUsersByFields($this->request->query->all('filters'), $this->request->query->get('offset'));
        return new JsonResponse($users);
    }

    #[Route('/createFakeUsers', name: 'createFakeUsers', methods: ['GET'])]
    public  function createFakeUsers(): Response
    {
        $fakeData = new FakeData();
        $users = $fakeData->getFakeData();
        $this->usersRepository->saveUsers($users);
        return new Response(null, 200);
    }
}
