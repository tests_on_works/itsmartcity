<?php

namespace App\Entity\Core;

use App\Repository\Core\UsersRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UsersRepository::class)]
class Users
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    public ?int $id_user = null;

    #[ORM\Column]
    public ?string $first_name;

    #[ORM\Column]
    public ?string $second_name;

    #[ORM\Column]
    public ?string $middle_name;

    #[ORM\Column]
    public ?bool $is_active;

    #[ORM\Column]
    public ?DateTime $last_visitors;

    #[ORM\Column]
    public ?DateTime $date_created;

    #[ORM\Column]
    public ?DateTime $date_updated;

    public function __construct($first_name, $second_name, $middle_name, $is_active, $last_visitors)
    {
        $this->first_name = $first_name;
        $this->second_name = $second_name;
        $this->middle_name = $middle_name;
        $this->is_active = $is_active;
        $this->last_visitors = $last_visitors;
        $this->date_created = new DateTime();
        $this->date_updated = new DateTime();
    }
}
