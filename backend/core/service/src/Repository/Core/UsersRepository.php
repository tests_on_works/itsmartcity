<?php

namespace App\Repository\Core;

use App\Entity\Core\Users;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Users>
 *
 * @method Users find($id, $lockMode = null, $lockVersion = null)
 * @method Users findOneBy(array $criteria, array $orderBy = null)
 * @method Users[]    findAll()
 * @method Users[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class UsersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Users::class);
    }

    /**
     * @param Users[] $users$
     * @return void
     */
    public function saveUsers(array $users) {

        foreach ($users as $user)
        {
            $this->_em->persist($user);
        }
        $this->_em->flush();

    }

    public function getUsers($offset)
    {
        $result = [];
        $result['count'] = count($this->findAll());
        $result['users'] = $this->findBy([],[], 25, $offset);
        return $result;
    }

    /**
     * @param $filters
     * @return Users[]
     */
    public function getUsersByFields($filters, $offset): array
    {
        $result = [];
        $query = $this->createQueryBuilder('u')->select();
        foreach ($filters as $key => $param)
        {
            switch ($key)
            {
                case 'first_name':
                case 'second_name':
                case 'middle_name':
                    $query->andWhere("u.$key LIKE :$key");
                    $query->setParameter("$key", '%'.$param.'%');
                    break;
                case 'is_active':
                    $query->andWhere("u.$key = :$key");
                    $query->setParameter("$key", $param);
                    break;
                case 'last_visitors':
                    $query->andWhere("u.$key > :startDate");
                    $query->setParameter("startDate", new DateTime($param[0]));
                    $query->andWhere("u.$key < :endDate");
                    $query->setParameter("endDate", new DateTime($param[1]));
                    break;

            }
        }
        $query->addOrderBy("u.${filters['propertySort']}", "${filters['sortType']}");

        if($filters['propertySort'] !== 'id_user')
        {
            $query->addOrderBy("u.id_user", "${filters['sortType']}");
        }

        $result['count'] = ceil(count($query->getQuery()->getResult())/25);

        $query->setMaxResults(25);
        $query->setFirstResult($offset);

        $result['users'] = $query->getQuery()->getResult();

        return $result;
    }
}
