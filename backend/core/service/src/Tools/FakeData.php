<?php
namespace App\Tools;

use App\Entity\Core\Users;
use DateTime;

class FakeData
{
    private $firstNames = ['Кирилл', 'Иван', 'Николай', 'Дмитрий', 'Сергей', 'Денис', 'Петр'];
    private $secondNames = ['Кузнецов', 'Иванов', 'Сидоров', 'Петров', 'Бухарин', 'Мазепин', 'Иванчук'];
    private $middleNames = ['Дмитриевич', 'Сергеевич', 'Николаевич', 'Петрович', 'Денисович', 'Иванович', 'Сидорович'];

    public function getFakeData() {
        $results = [];
        $iterator = 0;
        while($iterator < 500)
        {
            $date = new DateTime();
            $date->setDate(rand(2020, 2023), rand(1,12), rand(1, 28));


            $user = new Users(
                $this->firstNames[rand(0, 6)],
                $this->secondNames[rand(0, 6)],
                $this->middleNames[rand(0, 6)],
                (bool)rand(0,1), $date);

            $results[] = $user;
            $iterator++;
        }

        return $results;
    }
}
