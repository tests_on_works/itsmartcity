import axios from '@/plugins/axios';

export default class ApiService {

    constructor() {

    }
    static async getUsers(){
        const { data } = await axios.get('/users');
        return data;
    }

    static async createTestData(){
        const { data } = await axios.get('/createFakeUsers');
        return data;
    }

    static async getUserByFileds(fields){
        if (this.abortController) {
            this.abortController.abort();
        }
        this.abortController = new AbortController();
        const signal = this.abortController.signal;
        const { data } = await axios.get('/usersByField', {signal, params: fields});
        return data;

    }
}
