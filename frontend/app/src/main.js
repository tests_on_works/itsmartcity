import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import VueDatePicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css'


import App from './App.vue'
import router from './router'
import VueMultiselect from 'vue-multiselect'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.component('multiselect', VueMultiselect);
app.component('VueDatePicker', VueDatePicker);

app.mount('#app')
